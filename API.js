const express =  require("express");
const { Pool } = require("pg");

const app = express();
const port = 3000;

const pool = new Pool({
    user: 'postgres',
    password: 'postgres',
    host: 'localhost',
    database: 'postgres',
    port: '5432',
})
const Usuario = {
    cedula_identidad: String,
    nombre: String,
    primer_apellido: String,
    segundo_apellido: String,
    fecha_nacimiento: Date
};
// Modelo
class Model{


    async getUsuarios(){
        const { rows } = await pool.query("select * from usuarios;");
        return rows;
    }

    async getUsuario(id){
        const { rows } = await pool.query("select * from usuarios where cedula_identidad=$1;", [id]);
        return rows[0];
    }

    async addUsuario(user) {
        await pool.query("insert into usuarios (cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento) VALUES ($1, $2, $3, $4, $5);", [user])
    }

    async updateUsuario(id, user) {
        await pool.query("UPDATE usuarios SET cedula_identidad = $1, nombre = $2, primer_apellido = $3, segundo_apellido = $4, fecha_nacimiento = $5 WHERE cedula_identidad = $6;", [user, id])
    }

    async deleteUsuario(id) {
        await pool.query("delete from usuarios where cedula_identidad = $1;", [id]);
    }

    async getPromedio(id){
        const { rows } = await pool.query("SELECT AVG(EXTRACT(YEAR FROM AGE(NOW(), fecha_nacimiento))) AS promedio_edades FROM usuarios where cedula_identidad = $1;", [id]);
        return rows[0];
    }
}

// controlador
class Controller{
    constructor(model) {
        this.model = model;
    }

    async getUsuarios(req, res){
        const data = await this.model.getUsuarios();
        res.send(data);
    }

    async getUsuario(req, res){
        const id = req.params.id;
        const data = await this.model.getUsuario(id);
        res.send(data);
    }

    async addUsuario(req, res) {
        const user = Usuario.create(req.body);
        await this.model.addTodo(user);
        res.sendStatus(201);
    }

    async updateUsuario(req, res) {
        const id = req.params.id;
        const user = req.body.user;
        await this.model.updateTodo(id, user)
        res.sendStatus(200);
    }
    async deleteUsuario(req, res) {
        const id = req.params.id;
        await this.model.deleteTodo(id);
        res.sendStatus(204);
    }
    async getPromedio(req, res){
        const id = req.params.id;
        const data = await this.model.getPromedio(id);
        res.send(data);
    }
}

const model = new Model();
const controller = new Controller(model);

app.use(express.json());

// creación de rutas
app.get("/usuarios", controller.getUsuarios.bind(controller));
app.get("/usuarios/:id_usuarios", controller.getUsuario.bind(controller));
app.post("/usuarios", controller.addUsuario.bind(controller));
app.put("/usuarios/:id_usuarios", controller.updateUsuario.bind(controller));
app.delete("/usuarios/:id_usuarios", controller.deleteUsuario.bind(controller));
app.get("/usuarios/:promedio-edad", controller.getPromedio.bind(controller));


app.listen(port, () => {
    console.log(`Servidor levantado en http://localhost:${port}`)
});

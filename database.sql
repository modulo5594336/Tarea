CREATE TABLE usuarios (
  cedula_identidad VARCHAR(20) NOT NULL,
  nombre VARCHAR(100) NOT NULL,
  primer_apellido VARCHAR(100) NOT NULL,
  segundo_apellido VARCHAR(100) NOT NULL,
  fecha_nacimiento DATE NOT NULL
);

INSERT INTO usuarios (cedula_identidad, nombre, primer_apellido, segundo_apellido, fecha_nacimiento)
VALUES
  ('1234567', 'Gabriel', 'Lavayen', 'Pinto', '1995-04-28'),
  ('9876543', 'Pablo', 'Perez', 'Mendoza', '1990-02-02');